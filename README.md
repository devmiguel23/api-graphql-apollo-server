<p align="center"><a href="https://www.apollographql.com/docs/apollo-server/" target="_blank" rel="noopener noreferrer"><img width="100" src="https://graphql.org/img/logo.svg" alt="graphql"></a></p>

**Iniciar el servidor**
`npm i`
`.env`
`NPM RUN SERVER`

`query login($email: String!, $password: String!){
  login(email: $email, password: $password){
    userID
    token
  }
}`
`query allUsers($limit: Int, $withRole: Boolean!){
	users(limit: $limit){
    _id
    email
    password
    firstname
    lastname
    age
    role @include(if: $withRole){
      	_id
    }
    status
  }
}`
`mutation createUser($createUser:UsersInput){
  createUser(input: $createUser){
    _id
    email
    password
    firstname
    lastname
    age
    role{
      _id
    }
    status
  }
}`
