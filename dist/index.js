"use strict";

require("regenerator-runtime");

var _apolloServer = require("apollo-server");

var _database = require("./database");

var _jsonwebtoken = require("jsonwebtoken");

var _dotenv = _interopRequireDefault(require("dotenv"));

var _userSchema = _interopRequireDefault(require("./graphql/schema/userSchema"));

var _userResolvers = _interopRequireDefault(require("./graphql/resolvers/userResolvers"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_dotenv["default"].config(); //Schema


var server = new _apolloServer.ApolloServer({
  typeDefs: [_userSchema["default"]],
  resolvers: [_userResolvers["default"]],
  playground: true,
  introspection: true,
  cors: {
    origin: "*",
    credentials: true
  },
  context: function context(_ref) {
    var req = _ref.req;
    // get the user token from the headers
    var token = req.headers.authorization || '';

    function getUser(token, secretkey) {
      try {
        if (token) {
          var verified = (0, _jsonwebtoken.verify)(token, secretkey);
          return verified;
        }
      } catch (e) {
        throw new Error(e.message);
      }
    }

    var user = getUser(token, process.env.SECRETKET); // if (!user) throw new Error('Token Invalido');

    console.log(process.env.MIGUEL);
    return {
      user: user
    };
  }
});
(0, _database.mongoConnect)().then(function () {
  server.listen({
    port: process.env.PORT || 4000
  }).then(function (_ref2) {
    var url = _ref2.url;
    console.log("\u2705 Server listo en la ruta ".concat(url));
  });
});
//# sourceMappingURL=index.js.map