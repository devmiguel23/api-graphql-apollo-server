"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _Role = _interopRequireDefault(require("../models/Role"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var InicializacionSetup = function InicializacionSetup() {
  _classCallCheck(this, InicializacionSetup);
};

_defineProperty(InicializacionSetup, "createRoles", /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
  var count;
  return regeneratorRuntime.wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return _Role["default"].estimatedDocumentCount();

        case 3:
          count = _context.sent;

          if (!(count > 0)) {
            _context.next = 6;
            break;
          }

          return _context.abrupt("return");

        case 6:
          _context.next = 8;
          return Promise.all([new _Role["default"]({
            name: "user"
          }).save(), new _Role["default"]({
            name: "moderator"
          }).save(), new _Role["default"]({
            name: "admin"
          }).save()]);

        case 8:
          console.log('✅ La coleccion Roles fue creada junto con sus campos.');
          _context.next = 14;
          break;

        case 11:
          _context.prev = 11;
          _context.t0 = _context["catch"](0);
          throw new Error("\u274C Problemas al crear los roles. \n ".concat(_context.t0));

        case 14:
        case "end":
          return _context.stop();
      }
    }
  }, _callee, null, [[0, 11]]);
})));

var _default = InicializacionSetup;
exports["default"] = _default;
//# sourceMappingURL=InicializacionSetup.js.map