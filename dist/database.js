"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mongoConnect = mongoConnect;

var _mongoose = require("mongoose");

var _InicializacionSetup = _interopRequireDefault(require("./helper/InicializacionSetup"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function mongoConnect() {
  return _mongoConnect.apply(this, arguments);
}

function _mongoConnect() {
  _mongoConnect = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return (0, _mongoose.connect)("mongodb+srv://graphql:7OvYYFd2JiJ2M1gX@graphql-api-playground.cdmc3.mongodb.net/myFirstDatabase?retryWrites=true&w=majority", {
              useNewUrlParser: true,
              useUnifiedTopology: true,
              useCreateIndex: true,
              useFindAndModify: false
            }).then(function () {
              console.log("✅ Conectado a la base de datos MongoDB");

              _InicializacionSetup["default"].createRoles();
            })["catch"](function (e) {
              throw new Error("\u274C ".concat(e));
            });

          case 2:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _mongoConnect.apply(this, arguments);
}
//# sourceMappingURL=database.js.map