"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _User = _interopRequireDefault(require("../../models/User"));

var _Role = _interopRequireDefault(require("../../models/Role"));

var _jsonwebtoken = require("jsonwebtoken");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var userResolvers = {
  Query: {
    users: function () {
      var _users = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(_, args, context) {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _User["default"].find().limit(args.limit).then(function (users) {
                  if (!context.user) throw new Error("Token Invalido");
                  return users.map(function (user) {
                    return _objectSpread({}, user._doc);
                  });
                })["catch"](function (e) {
                  throw new Error(e);
                });

              case 2:
                return _context.abrupt("return", _context.sent);

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function users(_x, _x2, _x3) {
        return _users.apply(this, arguments);
      }

      return users;
    }(),
    login: function () {
      var _login = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(_, _ref) {
        var email, password, user, isEqual, token;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                email = _ref.email, password = _ref.password;
                _context2.next = 3;
                return _User["default"].findOne({
                  email: email
                });

              case 3:
                user = _context2.sent;

                if (user) {
                  _context2.next = 6;
                  break;
                }

                throw new Error("El usuario ".concat(email, " no existe!"));

              case 6:
                _context2.next = 8;
                return _User["default"].comparePassword(password, user.password);

              case 8:
                isEqual = _context2.sent;

                if (isEqual) {
                  _context2.next = 11;
                  break;
                }

                throw new Error("La contrase\xF1a no es corracta!");

              case 11:
                token = (0, _jsonwebtoken.sign)({
                  userID: user._id,
                  email: user.email
                }, process.env.SECRETKET, {
                  expiresIn: 60 * 60 * 12
                });
                return _context2.abrupt("return", {
                  userID: user._id,
                  token: token
                });

              case 13:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function login(_x4, _x5) {
        return _login.apply(this, arguments);
      }

      return login;
    }()
  },
  Mutation: {
    createUser: function createUser(_, _ref2, context) {
      return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
        var input, email, password, firstname, lastname, age, role, user, roleResponse, resRole, newUser;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                input = _ref2.input;
                // if (!context.user) throw new Error("Token Invalido");
                // console.log(context.user);
                email = input.email, password = input.password, firstname = input.firstname, lastname = input.lastname, age = input.age, role = input.role;
                _context3.next = 4;
                return _User["default"].findOne({
                  email: email
                });

              case 4:
                user = _context3.sent;

                if (!user) {
                  _context3.next = 7;
                  break;
                }

                throw new Error("El correo ".concat(user.email, " ya existe"));

              case 7:
                if (!role) {
                  _context3.next = 14;
                  break;
                }

                _context3.next = 10;
                return _Role["default"].find({
                  name: {
                    $in: role
                  }
                });

              case 10:
                resRole = _context3.sent;
                roleResponse = resRole.map(function (roles) {
                  return roles._id;
                });

                if (!(roleResponse.length == 0)) {
                  _context3.next = 14;
                  break;
                }

                throw new Error("Los permisos asignados no son validos.");

              case 14:
                _context3.t0 = _User["default"];
                _context3.t1 = email;
                _context3.next = 18;
                return _User["default"].encryptPassword(password);

              case 18:
                _context3.t2 = _context3.sent;
                _context3.t3 = firstname;
                _context3.t4 = lastname;
                _context3.t5 = age;
                _context3.t6 = roleResponse;
                _context3.t7 = {
                  email: _context3.t1,
                  password: _context3.t2,
                  firstname: _context3.t3,
                  lastname: _context3.t4,
                  age: _context3.t5,
                  role: _context3.t6
                };
                newUser = new _context3.t0(_context3.t7);
                _context3.next = 27;
                return newUser.save().then(function (res) {
                  return _objectSpread({}, res._doc);
                })["catch"](function (e) {
                  throw new Error(e);
                });

              case 27:
                return _context3.abrupt("return", _context3.sent);

              case 28:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    }
  }
};
var _default = userResolvers;
exports["default"] = _default;
//# sourceMappingURL=userResolvers.js.map