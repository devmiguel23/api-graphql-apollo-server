"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _apolloServer = require("apollo-server");

var _templateObject;

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var typeDefs = (0, _apolloServer.gql)(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    type Login{\n        userID: ID!\n        token: String!\n    }\n    type Role{\n        _id: ID!\n    }\n    input UsersInput {\n        email: String!\n        password: String!\n        firstname: String!\n        lastname: String\n        age: Int\n        role: [ID!]!\n        status: Boolean\n    }\n    type Users {\n        _id: ID\n        email: String\n        password: String\n        firstname: String\n        lastname: String\n        age: Int\n        role: [Role!]!\n        status: Boolean\n    }\n    type Query{\n        users(limit: Int): [Users!]!\n        login(email: String!, password: String!): Login!\n    }\n    type Mutation{\n        createUser(input: UsersInput): Users\n    }\n"])));
var _default = typeDefs;
exports["default"] = _default;
//# sourceMappingURL=userSchema.js.map