import 'regenerator-runtime'
import { ApolloServer, } from "apollo-server";
import { mongoConnect } from './database'
import { verify } from 'jsonwebtoken'
import dotenv from 'dotenv'
dotenv.config()

//Schema
import userSchema from "./graphql/schema/userSchema";
//Resolvers
import userResolvers from "./graphql/resolvers/userResolvers";

const server = new ApolloServer({
    typeDefs: [userSchema],
    resolvers: [userResolvers],
    playground: true,
    introspection: true,
    cors: {
        origin: "*",
        credentials: true
    },
    context: ({ req }) => {

        // get the user token from the headers
        const token = req.headers.authorization || '';
        function getUser(token, secretkey) {
            try {
                if (token) {
                    const verified = verify(token, secretkey);
                    return verified
                }
            } catch (e) {
                throw new Error(e.message)
            }
        }
        const user = getUser(token, process.env.SECRETKET);
        // if (!user) throw new Error('Token Invalido');
        return { user };
    }
});

mongoConnect().then(() => {
    server.listen({ port: process.env.PORT || 4000 }).then(({ url }) => {
        console.log(`✅ Server listo en la ruta ${url}`);
    });
});

