import { connect } from "mongoose";
import InicializacionSetup from "./helper/InicializacionSetup";

export async function mongoConnect() {

    await connect(process.env.MONGO_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false
    }).then(() => {
        console.log("✅ Conectado a la base de datos MongoDB");
        InicializacionSetup.createRoles();
    }).catch(e => {
        throw new Error(`❌ ${e}`);
    });
}