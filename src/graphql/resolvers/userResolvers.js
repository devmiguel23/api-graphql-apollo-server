import User from "../../models/User";
import Role from "../../models/Role"
import { sign } from 'jsonwebtoken'

const userResolvers = {
    Query: {
        users: async (_, args, context) => await User.find().limit(args.limit).then(users => {
            if (!context.user) throw new Error("Token Invalido");
            return users.map(user => {
                return { ...user._doc };
            });
        }).catch(e => {
            throw new Error(e);
        }),
        login: async (_, { email, password }) => {
            const user = await User.findOne({ email });
            if (!user) throw new Error(`El usuario ${email} no existe!`);
            const isEqual = await User.comparePassword(password, user.password);
            if (!isEqual) throw new Error(`La contraseña no es corracta!`);
            const token = await sign({ userID: user._id, email: user.email }, process.env.SECRETKET, { expiresIn: 60 * 60 * 12 });
            return {
                userID: user._id,
                token
            }
        }
    },
    Mutation: {
        async createUser(_, { input }, context) {
            // if (!context.user) throw new Error("Token Invalido");
            // console.log(context.user);
            const { email, password, firstname, lastname, age, role } = input;
            const user = await User.findOne({ email });
            if (user) throw new Error(`El correo ${user.email} ya existe`);

            let roleResponse;
            if (role) {
                const resRole = await Role.find({ name: { $in: role } });
                roleResponse = resRole.map(roles => roles._id);
                if (roleResponse.length == 0) throw new Error("Los permisos asignados no son validos.")
            }

            const newUser = new User({ email, password: await User.encryptPassword(password), firstname, lastname, age, role: roleResponse });
            return await newUser.save().then(res => {
                return { ...res._doc };
            }).catch(e => {
                throw new Error(e);
            });

        },
    }
}
export default userResolvers