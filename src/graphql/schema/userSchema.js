import { gql } from "apollo-server";

const typeDefs = gql`
    type Login{
        userID: ID!
        token: String!
    }
    type Role{
        _id: ID!
    }
    input UsersInput {
        email: String!
        password: String!
        firstname: String!
        lastname: String
        age: Int
        role: [ID!]!
        status: Boolean
    }
    type Users {
        _id: ID
        email: String
        password: String
        firstname: String
        lastname: String
        age: Int
        role: [Role!]!
        status: Boolean
    }
    type Query{
        users(limit: Int): [Users!]!
        login(email: String!, password: String!): Login!
    }
    type Mutation{
        createUser(input: UsersInput): Users
    }
`;

export default typeDefs