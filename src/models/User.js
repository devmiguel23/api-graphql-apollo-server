import { Schema, model } from "mongoose";
import { genSalt, hash, compare } from 'bcryptjs';

const userSchema = new Schema({
    email: {
        type: String,
        trim: true,
        lowercase: true,
        unique: true,
    },
    password: {
        type: String,
        required: true
    },
    firstname: {
        type: String
    },
    lastname: {
        type: String
    },
    age: {
        type: Number
    },
    role: [
        {
            ref: "Role",
            type: Schema.Types.ObjectId,
        }
    ],
    status: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: true,
    versionKey: false
});

//Encrypt Password
userSchema.statics.encryptPassword = async (password) => {
    const salt = await genSalt(10);
    return await hash(password, salt);
}
//Compare Password
userSchema.statics.comparePassword = async (password, receivedPassword) => {
    return await compare(password, receivedPassword);
}

export default model("User", userSchema);