import Role from "../models/Role";

class InicializacionSetup {
    static createRoles = async () => {
        try {
            const count = await Role.estimatedDocumentCount();
            // check for existing roles
            if (count > 0) return;

            // Create default Roles
            await Promise.all([
                new Role({ name: "user" }).save(),
                new Role({ name: "moderator" }).save(),
                new Role({ name: "admin" }).save(),
            ]);
            console.log('✅ La coleccion Roles fue creada junto con sus campos.');

        } catch (e) {
            throw new Error(`❌ Problemas al crear los roles. \n ${e}`);
        }
    }
}

export default InicializacionSetup;